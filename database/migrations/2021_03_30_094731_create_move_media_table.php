<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoveMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('move_media', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('move_id');
            $table->foreign('move_id')->references('id')->on('moves');
            $table->unsignedBigInteger('media_type_id');
            $table->foreign('media_type_id')->references('id')->on('media_types');
            $table->string('info')->nullable();
            $table->string('link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('move_media');
    }
}
