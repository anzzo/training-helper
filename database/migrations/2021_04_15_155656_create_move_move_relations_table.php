<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoveMoveRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('move_move_relations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('move_relation_type_id');
            $table->foreign('move_relation_type_id')->references('id')->on('move_relation_types');
            $table->unsignedBigInteger('move_id');
            $table->foreign('move_id')->references('id')->on('moves');
            $table->unsignedBigInteger('child_move_id');
            $table->foreign('child_move_id')->references('id')->on('moves');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('move_move_relations');
    }
}
