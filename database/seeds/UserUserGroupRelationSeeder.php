<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserUserGroupRelationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_user_group_relations')->insert([
        	'user_id'       => 1,
			'user_group_id' => 1,
            'updated_at'    => \Carbon\Carbon::now(),
            'created_at'    => \Carbon\Carbon::now(),
        ]);
    	foreach($users = User::where('id', '!=', 1)->cursor() as $index => $user){
			DB::table('user_user_group_relations')->insert([
	        	'user_id'       => $user->id,
				'user_group_id' => 2,
	            'updated_at'    => \Carbon\Carbon::now(),
	            'created_at'    => \Carbon\Carbon::now(),
	        ]);
    	}
    }
}
