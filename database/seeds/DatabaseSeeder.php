<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
        	UserGroupSeeder::class,
        	UserSeeder::class,
        	UserUserGroupRelationSeeder::class,
        	LanguageSeeder::class,
        	MoveSeeder::class,
        	WorkoutSeeder::class,
        	UserWorkoutRelationSeeder::class,
        ]);
    }
}
