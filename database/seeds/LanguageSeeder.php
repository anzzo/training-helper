<?php

use Illuminate\Database\Seeder;
use App\Models\Language;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$base_data = [['FI', 'Suomi'], ['SV', 'Ruotsi'], ['EN', 'Englanti']];
    	$id = (Language::max('id') ?: 0) + 1;
    	$languages = [];
    	$translations = [];
    	foreach($base_data as $index => $lan){
    		$language = [
    			'id'         => $id++,
    			'short_name' => $lan[0],
    			'updated_at' => \Carbon\Carbon::now(),
	            'created_at' => \Carbon\Carbon::now(),
    		];
    		$languages[] = $language;
    		foreach($base_data as $subIndex => $data){
    			$name = '';
    			if($subIndex == 0){
    				$name = $lan[1];
    			}
    			$translation = [
    				'language_id'    => $language['id'],
					'translation_id' => ($subIndex + 1),
					'name'           => $name,
					'updated_at'     => \Carbon\Carbon::now(),
	            	'created_at'     => \Carbon\Carbon::now(),
    			];
    			$translations[] = $translation;
    		}
    	}

    	$chunks = array_chunk($languages, 100);
    	foreach($chunks as $index => $chunk){
    		DB::table('languages')->insert($chunk);
    	}
    	$chunks = array_chunk($translations, 100);
    	foreach($chunks as $index => $chunk){
    		DB::table('language_translations')->insert($chunk);
    	}
    }
}
