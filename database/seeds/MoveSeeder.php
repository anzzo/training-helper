<?php

use Illuminate\Database\Seeder;
use App\Models\Move;
use App\Models\Language;

class MoveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $moves = [
			"Calf-Raise",
			"Squat Calf-Raise",
			"Single-Leg Calf-Raise",
			"Stiff-Leg Ankle Hop",
			"Wall Sit",
			"Sumo Squat",
			"Air Squat",
			"Step-Up",
			"Static Lunge",
			"Forward Lunge",
			"Reverse Lunge",
			"Bulgarian Split Squat",
			"Single-Leg Box Squat",
			"Skater Squat",
			"Shrimp Squat",
			"Pistol Squat",
			"Glute Bridge",
			"Side Lying Clam",
			"Donkey Kick",
			"Bent-Leg Donkey Kick",
			"Single-Leg Romanian Deadlift",
			"Single-Leg Glute Bridge",
			"Shoulders-Elevated Hip Raise",
			"Single-Leg Shoulders Elevated Hip Raise",
			"Russian Leg Curl",
			"Shoulders-and-Feet-Elevated Hip Raise",
			"Single-Leg Shoulders-and-Feet-Elevated Hip Raise",
			"Sit-Up ",
			"Twisting Sit-Up",
			"Crunch",
			"Reverse Crunch",
			"Side Crunch",
			"Flutter Kick",
			"Bicycle",
			"Superman",
			"Bird Dog",
			"Dead Bug",
			"Plank",
			"Feet-Elevated Plank",
			"Side Plank",
			"Feet-Elevated Side Plank",
			"RKC Plank",
			"Lying Straight-Leg Raise",
			"Hanging Knee-Tuck",
			"Hanging Leg-Raise",
			"Oblique Hanging Leg-Raise",
			"Windshield Wiper",
			"L-Sit",
			"Dragon Flag",
			"Doorway Row ",
			"Inverted Curl",
			"Inverted Row",
			"Eccentric Chin/Pull-Ups",
			"Chin-Up",
			"Pull-Up",
			"Feet-Elevated Inverted Row",
			"Side-To-Side Pull-Up",
			"One-Arm Inverted Row",
			"One-Arm Chin-Up",
			"YTWL",
			"Wall Push-Up",
			"Torso-Elevated Push-Up",
			"Push-Up",
			"Bench Dip",
			"Push-Back",
			"Spiderman Push-Up",
			"Triceps Extension",
			"Feet-Elevated Push-Up",
			"Chest Dip",
			"Hindu Push-Up",
			"Diamond Push-Up",
			"Wide Push-Ups",
			"Side-To-Side Push-Up ",
			"Feet-Elevated Pike Push-Up",
			"Assisted One-Arm Push-Up",
			"Wall Handstand Push-Up",
			"One-Arm Push-Up",
			"Crab Walk",
			"Inchworm",
			"Bear Crawl",
			"Crocodile Crawl",
			"Mountain Climber",
			"Frog Stand",
			"Planche",
			"Back Lever",
			"Front Lever",
			"Muscle-Up",
			"Human Flag",
			"Jumping Jack",
			"Jumping Rope",
			"Jump Squat",
			"Lateral Jump",
			"Tuck Jump",
			"Jumping Lunge",
			"Burpee ",
			"Knee-Slap Push-Up ",
			"Single-Leg Jump Squat",
			"Flying Superman Push-Up",
			"Aztec Push-Up"
		];
		foreach($moves as $index => $move){
			DB::table('moves')->insert([
	            'repetition'      => 10,
	            'creator_user_id' => null,
	            'updated_at'      => \Carbon\Carbon::now(),
	            'created_at'      => \Carbon\Carbon::now(),
	        ]);			
		}
        foreach(Language::cursor() as $index => $language){
	        foreach(Move::cursor() as $subIndex => $move){
	        	$name = '';
	        	if($language->short_name == 'EN'){
	        		$name = $moves[$subIndex];
	        	}
		        DB::table('move_translations')->insert([
		            'language_id' => $language->id,
					'move_id'     => $move->id,
					'name'        => $name,
					'description' => 'Lorem ipsum dolor sit amet, ne assum epicuri duo, pri sumo euripidis disputando eu. Mea ut singulis oportere. Te sit vidit platonem, rebum invenire sit te. An nam luptatum vituperatoribus, ne mei persius nusquam, has in natum concludaturque.

            Ut lucilius posidonium quo. Eam error facilisi tractatos te. At melius inciderint mei, reprimique consequuntur vim cu. Integre docendi neglegentur vix in. No numquam consetetur his, mea libris omnesque ea. Omnis vocibus sed id, eos ad modo habemus, sed ut maiorum theophrastus.',
					'updated_at' => \Carbon\Carbon::now(),
	            	'created_at' => \Carbon\Carbon::now(),
		        ]);
	        }        	
        }
    }
}
