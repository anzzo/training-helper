<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Workout;

class UserWorkoutRelationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$min_id = User::min('id');
    	$max_id = User::max('id');
		foreach(Workout::cursor() as $index => $workout){
	        DB::table('user_workout_relations')->insert([
	        	'user_id'       => rand($min_id, $max_id),
				'workout_id'    => $workout->id,
	            'updated_at'    => \Carbon\Carbon::now(),
	            'created_at'    => \Carbon\Carbon::now(),
	        ]);    				
		}
    }
}
