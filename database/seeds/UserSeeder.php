<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $token = Str::random(60);
    	DB::table('users')->insert([
            'firstname'      => 'Admin',
            'lastname'       => 'Training',
            'email'          => 'admin@admin.fi',
            'password'       => hash('sha256', 'admin'),
            'user_group_id' => \App\Models\User_group::where('type', 'admin')->first()->id,
            'api_token'      => hash('sha256', $token),
            'updated_at'     => \Carbon\Carbon::now(),
            'created_at'     => \Carbon\Carbon::now(),
        ]);
    	for($i = 0; $i < 10; $i++){
            $token = Str::random(60);
	        DB::table('users')->insert([
	            'firstname'      => Str::random(10),
	            'lastname'       => Str::random(10),
	            'email'          => Str::random(10).'@gmail.com',
	            'password'       => hash('sha256', 'password'),
                'user_group_id' => \App\Models\User_group::where('type', 'default')->first()->id,
                'api_token'      => hash('sha256', $token),
                'updated_at'     => \Carbon\Carbon::now(),
                'created_at'     => \Carbon\Carbon::now(),
	        ]);    		
    	}
    }
}
