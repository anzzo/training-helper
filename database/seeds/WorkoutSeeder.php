<?php

use Illuminate\Database\Seeder;
use App\Models\Move;
use App\Models\Workout;
use App\Models\Language;

class WorkoutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$languages = Language::get();
    	$workout_id = (Workout::max('id') ?: 0) + 1;
    	$workouts = [];
    	$relations = [];
    	$translations = [];
    	$chunks = Move::get()->chunk(3);
    	foreach($chunks as $index => $chunk){
    		$workout = [
    			'id'              => $workout_id++,
    			'creator_user_id' => null,
				'partition'       => rand(1,5),
				'updated_at'      => \Carbon\Carbon::now(),
            	'created_at'      => \Carbon\Carbon::now(),
    		];
    		$workouts[] = $workout;
    		foreach($chunk as $subIndex => $move){
    			$relation = [
					'workout_id' => $workout['id'],
					'move_id'    => $move->id,
					'updated_at' => \Carbon\Carbon::now(),
	            	'created_at' => \Carbon\Carbon::now(),
				];
				$relations[] = $relation;
    		}
    		foreach($languages as $subIndex => $language){
    			$translation = [
					'language_id' => $language->id,
					'workout_id'  => $workout['id'],					
					'name'        => Str::random(10),
					'description' => "Lorem ipsum dolor sit amet, ne assum epicuri duo, pri sumo euripidis disputando eu. Mea ut singulis oportere. Te sit vidit platonem, rebum invenire sit te. An nam luptatum vituperatoribus, ne mei persius nusquam, has in natum concludaturque.

            Ut lucilius posidonium quo. Eam error facilisi tractatos te. At melius inciderint mei, reprimique consequuntur vim cu. Integre docendi neglegentur vix in. No numquam consetetur his, mea libris omnesque ea. Omnis vocibus sed id, eos ad modo habemus, sed ut maiorum theophrastus.",
					'updated_at'  => \Carbon\Carbon::now(),
	            	'created_at'  => \Carbon\Carbon::now(),
    			];
    			$translations[] = $translation;
    		}
    	}
    	$chunks = array_chunk($workouts, 100);
    	foreach($chunks as $index => $chunk){
    		DB::table('workouts')->insert($chunk);
    	}
    	$chunks = array_chunk($relations, 100);
    	foreach($chunks as $index => $chunk){
    		DB::table('workout_move_relations')->insert($chunk);
    	}
    	$chunks = array_chunk($translations, 100);
    	foreach($chunks as $index => $chunk){
    		DB::table('workout_translations')->insert($chunk);
    	}
    }
}
