<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'v1'], function(){
	/* Public routes */	
	Route::group(['prefix' => 'public'], function(){
		Route::get('/login', 'ApiController@login')->name('login');
		Route::post('/logout', 'ApiController@logout')->name('logout');
		Route::post('/register', 'ApiController@register')->name('register');
		Route::get('/{model}', 'ApiController@default');
	});

	/* Routet joihin vaaditaan sisäänkirjautuminen */
	Route::group(['prefix' => 'authenticated'], function(){
		Route::get('/self', 'ApiController@getUser');
		/* Täällä kaikki luontiin liittyvät routet esim. /authenticated/create/move */
		Route::group(['prefix' => 'create'], function(){
			Route::post('/{model}', 'ApiController@authenticated');
		});
	});
	
	/* Authenticated routes */
	Route::middleware('auth:api', function(){
		/* Admin routes */
		Route::group(['prefix' => 'auth'], function(){
			Route::any('/{model}', 'ApiController@authenticated');
		});
		Route::group(['prefix' => 'admin'], function(){
			Route::any('/{model}', 'ApiController@admin');
		});
	});
	
});
