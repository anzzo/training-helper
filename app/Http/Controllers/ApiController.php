<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Move;
use App\Tools\ModelHelpers;
use Illuminate\Support\Str;
use DB;

class ApiController extends Controller
{
    public function default(Request $request, $model){
        /* First check that current method is allowed */
        $config = config('api');
        $permissions = $config['permissions']['default'];
        return $this->index($request, $model, $permissions);
    }

    public function authenticated(Request $request, $model){
        /* First check that current method is allowed */
        $config = config('api');
        $permissions = $config['permissions']['authenticated'];
        if(!self::isAuthenticated($request->api_token)) return null;
        return $this->index($request, $model, $permissions);
    }

    public function admin(Request $request, $model){
        /* First check that current method is allowed */
        $config = config('api');
        $permissions = $config['permissions']['admin'];
        if(!self::isAdmin($request->api_token)) return null;
        return $this->index($request, $model, $permissions);
    }

    public function index(Request $request, $model, $permissions){
        // info(['request' => $request, 'model' => $model, 'permissions' => $permissions]);
    	/* First check that current method is allowed */
    	$method = $request->method();
    	/* Then check if searched model exists */
    	$model = ucfirst($model);
        info('Pyydetään dataa modelista: ' . $model);
    	if(!in_array($method, $permissions['allowed_methods'])) return null;
        if(!in_array($model, $permissions['allowed_models'])) return null;

    	$model = "App\Models\\$model";
    	if(!class_exists($model)) return null;
    	/* Now we have confirmed that model exists and method is allowed */
    	/* Next we need to take right action depending which method we are using */

    	/* Firstly lets check that we have handler for our method */
    	$method = strtolower($method);
    	$function = $method.'Handler';
    	if(!is_callable($function, true)) return null;
    	/* Call handler and pass arguments */
    	$ret = $this->$function($model, $request);
    	return $ret;
    }

    /**
     * Data muodossa
     *  $request = [
     *      terms =>[
     *          'where' => [
     *              'field' => 'value'
     *          ] 
     *      ],
     *      relations => [
     *          'relation1',
     *          'relation2',
     *      ] 
     *  ]
     * 
     * */
    public function getHandler($model, $request){
        /* Start the query */
        $query = $model::query();
        /* Go trough limiting terms and add them to query */
        if($request->terms){
            foreach($request->terms as $term => $values){
                foreach($values as $field => $value){
                    $query->$term($field, $value);
                }
            }
        }

        /* Load all wanted relations */
        if($request->relations){
            foreach($request->relations as $index => $relation){
                $query->with($relation);
            }
        }
        info('palautetaan dataa modelista: ' . $model);
        // info($query->get()->toJson());
        return $query->get()->toJson();
    }

    public function postHandler($model, $request){
        /*
            $request ==
            [
                'token' => 'xyz',
                'models' => [
                    [
                        <field_name> => <field_value>
                        <field_name> => <field_value>
                        .
                        .
                        .
                    ],
                    .
                    .
                    .
                ]
            ]
        */
        info('Luodaan dataa modeliin: ' . $model);
        $created = [];
        foreach($request->models as $index => $values){
            $m = new $model;
            foreach($values as $field => $value){
                $m->$field = $value;
            } 
            $m->save();
            $created[] = $m->toArray();
        }
        info('Luotiin ' . ($index + 1) . ' modelia');
        /*
            Returns array of created models
        */
        info($created);
    	return json_encode($created);
    }

    public function putHandler($model, $request){
    	return "Not yet implemented";
    }

    public function patchHandler($model, $request){
    	return "Not yet implemented";
    }

    public function login(Request $request){
        $credentials = $request->only('email', 'password');
        if (($user = User::where('email', $request->email)->where('password', hash('sha256', $request->password))->first())){
            return ['token' => $user->api_token];
        }
        return ['token' => null];
    }

    public function logout(Request $request){
        $user = User::where('api_token', $request->api_token)->first();
        if(!$user) return;
        $user->api_token = User::generateApiToken();
        $user->save();        
    }

    public function register(Request $request){
        DB::beginTransaction();
        try{
            if($request->password !== $request->password_again) throw new \Exception('Passwords didn\'t match');
            $user = new User;
            $user->firstname = $request->first_name ?: null; 
            $user->lastname  = $request->last_name ?: null; 
            $user->email     = $request->user_name; 
            $user->password  = hash('sha256', $request->password);
            $user->api_token = hash('sha256', Str::random(60));;
            if(!$user->save()) throw new \Exception('User creation failed');
            DB::commit();
            return ['success' => 1, 'message' => 'User created', 'data' => ['token' => $user->api_token]];
        }catch(\Exception $e){
            info('Exception in ApiController@register');
            info($e);
            DB::rollback();
            return ['success' => 0, 'message' => $e->getMessage(), 'data' => []];
        }
    }

    public function getUser(Request $request){
        return User::where('api_token', $request->api_token)->firstOrFail()->toJson();
    }

    public static function isAuthenticated($api_token){
        return User::where('api_token', $api_token)
                    ->where('api_token', '!=', null)
                    ->exists();
    }

    public static function isAdmin($api_token){
        $admin_group = User_group::where('type', 'admin')->first();
        if(!$admin_group) return false;
        return User::where('api_token', $api_token)
                    ->where('api_token', '!=', null)
                    ->where('user_group_id', $admin_group->id)
                    ->exists();
    }
}
