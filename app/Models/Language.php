<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    public function translations(){
        return $this->hasMany('\App\Models\Move_translation');
    }

    public function translation(){
        return $this->hasOne('\App\Models\Language_translation')->where(function($q){
            $q->where('language_id', 1);
            $q->where('name', '!=', '');
        })->orWhere('name', '!=', '');
    }
}
