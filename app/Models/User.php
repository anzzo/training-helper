<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class User extends Model
{
    public function getAuthIdentifier(){
    	return $this->attributes['id'];
    }

    public static function generateApiToken(){
    	return hash('sha256', Str::random(60));
    }
}
