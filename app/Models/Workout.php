<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Workout extends Model
{
    public function translations(){
    	return $this->hasMany('\App\Models\Workout_translation');
    }

    public function translation(){
    	return $this->hasOne('\App\Models\Workout_translation')->where(function($q){
    		$q->where('language_id', 1);
    		$q->where('name', '!=', '');
    	})->orWhere('name', '!=', '');
    }

    public function moves(){
    	return $this->belongsToMany('\App\Models\Move', 'workout_move_relations', 'workout_id', 'move_id');
    }
}
