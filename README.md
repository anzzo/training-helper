# API - rajapinta
Rajapinta on toteutettu REST pohjaisesti. Rajapinnan kaikki url:t löytää /routes/api.php tiedostosta. url:t on jaettu kolmeen eri kategoriaan: julkisiin, tunnistautumisen vaativiin ja admin. Rajapinnan kaikki logiikka sijaitsee /app/Http/Controllers/ApiController.php-tiedostossa, jossa on hyödynnetty PHP:n dynaamisuutta.

# Rajapinnan hallinta
Rajapinnan hallinta tapahtuu muokkaamalla /config/api.php-tiedoston avulla. Täällä on määritelty kaikki sallitut toimet eri tasoille.

# Tietokanta
Tietokanta luokat löytyvät app/Models-kansiosta. Tälllä on jokaista tietokantataulua kohden oma luokka.

# Aloitus
Lataa projekti omalle koneelle ja:
* Päivitä kirjastot
* Aja migraatiot ja luo data: php artisan migrate:fresh --seed
* Luo autentikointi tunnukset. Tähän ohjeet löytyvät täältä: https://laravel.com/docs/8.x/passport
