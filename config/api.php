<?php

return [
	'permissions' => [
		'default' => [
			'allowed_methods' => [
				'GET'
			],
			'allowed_models' => [
				'Workout',
				'Move',
				'Move_translation',
				'Language',
				'Workout_translation',
				'Workout_move_relation',
			],
		],
		'authenticated' => [
			'allowed_methods' => [
				'GET',
				'PUT',
				'PATCH',
				'POST'
			],
			'allowed_models' => [
				'Workout',
				'Move',
				'Move_translation',
				'Language',
				'Workout_translation',
				'Workout_move_relation',
			],
		],
		'admin' => [
			'allowed_methods' => [
				'GET',
				'PUT',
				'PATCH',
				'POST',
				'DELETE'
			],
			'allowed_models' => [
				'Workout',
				'Move',
				'Move_translation',
				'Language',
				'Workout_translation',
				'Workout_move_relation',
			],
		],
	]
];